import 'dart:io';
import 'package:flutter/material.dart';
import 'package:formvalidation/src/bloc/provider.dart';

import 'package:formvalidation/src/models/producto_model.dart';
//import 'package:formvalidation/src/provider/productos_provider.dart';

import 'package:formvalidation/src/utils/utils.dart' as utils;
import 'package:image_picker/image_picker.dart';

class ProductoPage extends StatefulWidget {
  @override
  _ProductoPageState createState() => _ProductoPageState();
}

class _ProductoPageState extends State<ProductoPage> {
 // final productoProvider = new ProductosProvider();
  final formKey = GlobalKey<FormState>();

  //variable para poder definir el snackBar en el scaffold, ya que
  //para mostrarlo se necesita de una  funcion.
  final scaffoldKey = GlobalKey<ScaffoldState>();
  
  ProductosBloc productosBloc;
  ProductoModel producto = new ProductoModel();
  bool _guardando = false;
  File foto;

  @override
  Widget build(BuildContext context) {
  
  productosBloc = Provider.productosBloc(context);
  
  
    //Con la siguiente linea de codigo lo que se hace es que se obtienen los
    //argumentos que vienen de otra pagina. en este caso de Home_page
    final ProductoModel prodData = ModalRoute.of(context).settings.arguments;

    if (prodData != null) {
      producto = prodData;
    }
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Producto'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.photo_size_select_actual),
            onPressed: _seleccionarFoto,
          ),
          IconButton(
            icon: Icon(Icons.camera_alt),
            onPressed: _tomarFoto,
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                _mostrarFoto(),
                _crearNombreProducto(),
                _crearPrecioProducto(),
                _crearSwitchDisponible(),
                _crearBotonEnProducto(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _crearNombreProducto() {
    return TextFormField(
      initialValue: producto.titulo,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Producto'),
      onSaved: (value) => producto.titulo = value,
      validator: (value) {
        if (value.length < 3) {
          return 'Ingrese el nombre del producto';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearPrecioProducto() {
    return TextFormField(
      initialValue: producto.valor.toString(),
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(labelText: 'Precio Producto'),
      // onSaved: (value) {
      // producto.valor = double.parse(value);

      // },
       onSaved: (value) => producto.valor = double.parse(value),
      validator: (value) {
        if (utils.isNumeric(value)) {
          return null;
        } else {
          return 'Solo Numeros';
        }
      },
     
    );
  }

  Widget _crearSwitchDisponible() {
    return SwitchListTile(
        value: producto.disponible,
        title: Text('Disponible'),
        activeColor: Colors.deepPurple,
        onChanged: (value) => setState(() {
              producto.disponible = value;
            }));
  }

  Widget _crearBotonEnProducto() {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      color: Colors.white,
      textColor: Colors.deepPurple,
      label: Text('Guardar'),
      icon: Icon(Icons.save),
      //Lo que hace la siguiente line de codigo seria: Si _guardando es true,
      //significa que se esta guardando informacion, entonces retorne
      //un null para que se deshabilite el boton,  en caso contrario
      //retorne el metodo _submit().
      onPressed: (_guardando) ? null : _submit,
    );
  }

  void _submit() async {
    if (!formKey.currentState.validate()) return;

    //Esta linea de codigo disparara todos los onsave:() de los
    //TextFormField().
    formKey.currentState.save();

    setState(() {
      _guardando = true;
    });

    //Carga la imagen en cloudinary y actualiza en firebase para que 
    //los registros tengan la url de la foto.
    if (foto != null) {
      producto.fotoUrl = await productosBloc.subirFoto(foto);
      print("entro a producto.fotoUrl =  ${producto.fotoUrl}");
    }

      //Si el id es nulo crea un producto, de lo contrario lo carga para editarlo
    if (producto.id == null) {
      productosBloc.agregarProducto(producto);
    } else {
      productosBloc.editarProducto(producto);
    }

    //  a
    //Justo aqui, despues de grabar la informacion anterior muestro el snackbar
    mostrarSnackbar('Registro Guardado');

    //Con esta instruccion nos sacaria de la ventana de actualizacion
    //o edicion de datos, llevandonos hacia atras en la pila de
    //actividades o ventanas.

    Navigator.pop(context);
  }

  void mostrarSnackbar(String mensaje) {
    final snackbar = SnackBar(
      content: Text(mensaje),
      duration: Duration(milliseconds: 1500),
    );

    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  Widget _mostrarFoto() {
  
    if (producto.fotoUrl != null) {
    
      return FadeInImage(
        image: NetworkImage(producto.fotoUrl),
        placeholder: AssetImage('assets/jar-loading.gif'),
        height: 300.0,
        fit: BoxFit.cover,
      );
    } else {
    
      return Image(
        //si la foto tiene un valor  y si esa foto tiene el path,
        //la foto se mostrara, pero si no tiene informacion o es null
        //utiliza la foto cargada en el assets/no-image.png.
        image: AssetImage(foto?.path ?? 'assets/no-image.png'),
        height: 300.0,
        fit: BoxFit.cover,
      );
    }
  }

  _seleccionarFoto() async {
    _procesarImagen(ImageSource.gallery);
  }

  _tomarFoto() async {
    _procesarImagen(ImageSource.camera);
  }

  _procesarImagen(ImageSource origen) async {
    foto = await ImagePicker.pickImage(
    source: origen
    
    );
    if (foto != null) {
      producto.fotoUrl = null;
    }
    //redibujamos el Widget nuevamente.
    setState(() {});
  }
}
