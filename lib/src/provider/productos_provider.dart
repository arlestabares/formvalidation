import 'dart:convert';
import 'dart:io';
import 'package:formvalidation/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

import 'package:mime_type/mime_type.dart';
import 'package:formvalidation/src/models/producto_model.dart';


class ProductosProvider {
//Con la siguiente linea fire expone su rest API y podemos hacer peticiones
//GET, PUT, POST , DELETE directamente como si fuera un servicio.
  final String _url = 'https://flutter-varios-8e5dd.firebaseio.com';
  
  final _prefUsuario= new PreferenciasUsuario();

//Funcion encargada de insertar los valores al firebase
  Future<bool> agregarProducto(ProductoModel producto) async {
    final url = '$_url/productos.json?auth=${_prefUsuario.token}';

//En la siguiente direccion hare la peticion post, para incertar un nuevo
//registro.
//Aqui mando  el producto al productoModelToJson() como un String, ya que dicha
//funcion regresa el modelo como un String y  se encuentra definido  en la clase
//ProductoModel(), clase que contiene el json.
//
    final resp = await http.post(url, body: productoModelToJson(producto));

    final decodedData = json.decode(resp.body);

    print(decodedData);

    //Aqui implicitamente con el async  retorna  un future, por ello no tengo
    // que poner que retorna un future que resuelve un true, porque eso lo esta
    //haciendo automaticamente  el async en este return true.
    return true;
  }
  
  Future<bool> editarProductoEnFirebase(ProductoModel producto) async {
    final url = '$_url/productos/${producto.id}.json?auth=${_prefUsuario.token}';
    final resp = await http.put(url, body: productoModelToJson(producto));

    final decodedData = json.decode(resp.body);

    print(decodedData);
    return true;
  }
  
  Future<List<ProductoModel>> cargarProductosDeFirebaseEnMovil() async {
    final url = '$_url/productos.json?auth=${_prefUsuario.token}';
    final resp = await http.get(url);

    //Este decodedData tinene un Mapa de Mapas, asi que necesito manipularlo
    //adecuadamente para extraer dicha informacion
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    final List<ProductoModel> productosDeFireBase = new List();

    if (decodedData == null) return [];
    
    //Esta es la validacion para saber si el token ya expiro.y por consiguiente 
    //retornaria la lista de productos vacia,,, aqui puedo mejorar esta  linea de codigo 
    //para mostrar algun mensaje al usuario.
    if  ( decodedData['error'] != null ) return [];

    //Con el forEach recorro cada uno de los id, y los productos que
    //que tengo en mi base datos. y para almacenarlos lo hare en la variable final
    //productos.
    decodedData.forEach((id, productos) {
      //Como es una mapa de mapas, el id del mapa interno no esta en el producto
      //por lo cual el id si esta en el primer mapa, de esta manera al producto
      //le asocio el id mas externo del mapa.
      final prodTemp = ProductoModel.fromJson(productos);
      prodTemp.id = id;

      productosDeFireBase.add(prodTemp);
      //Aqui imprimo los id que vienen de mi base de datos.
      print(prodTemp);
    });

    return productosDeFireBase;
  }

  Future<int> borrarProductosDeFireBase(String id) async {
    final url = '$_url/productos/$id.json?auth=${_prefUsuario.token}';

    final resp = await http.delete(url);

    print(resp.body);
    return 1;
  }


  //Subir fotografia tanto a Cloudinary como a firebase desde 
  //flutter con este nuevo servicio. implementando la siguiente funcion.
 
  Future<String> subirImagen(File imagen) async {
    final url = Uri.parse('http://api.cloudinary.com/v1_1/dqm7xcr55/image/upload?upload_preset=l4grwkcj');
    final mimeType = mime(imagen.path).split('/'); //imagen/jpeg
    
    final imageUploadRequest = http.MultipartRequest('POST', url);

    final file = await http.MultipartFile.fromPath('file', imagen.path,
        contentType: MediaType(mimeType[0], mimeType[1]));

    imageUploadRequest.files.add(file);

    final streamResponse = await imageUploadRequest.send();

    final resp = await http.Response.fromStream(streamResponse);

    //respuesta 200, y 201, son satisfactorias.
    if (resp.statusCode != 200 && resp.statusCode != 201) {
      print('Algo salio mal');
      print(resp.body);
      return null;
    }
    final respData = jsonDecode(resp.body);

    print(respData);

    return respData ['secure_url'];
  }
}
