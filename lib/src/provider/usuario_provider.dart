import 'dart:convert';

import 'package:formvalidation/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:http/http.dart' as http;

class UsuarioProvider {
  final String _firebaseToken = 'AIzaSyARZueLuEjpu6-sraetZngg_ntQENBTwWw';
  
  final _prefs = new PreferenciasUsuario();

  Future<Map<String, dynamic>> login(String email, String password) async {
    final authData = {
      'email': email,
      'password': password,
      'returnSecureToken': true
    };

    final resp = await http.post(
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=$_firebaseToken',
        body: json.encode(authData));
        
    Map<String, dynamic> decodedResp = json.decode(resp.body);
    print(decodedResp);

    if (decodedResp.containsKey('idToken')) {
    
      //salvar el token en el storage
      _prefs.token = decodedResp['idToken'];

      //si es verdadero, retorna un ok, y de paso muestra el idToken.
      return {'ok': true, 'token': decodedResp['idToken']};
    } else {
      //Caso contrario, muestra un false y en lugar del token muestra un
      //mensaje con el error, ya que dentro del objeto de la respuesta en su
      //propiedad error, buscara algo llamado message, ya que uno es un mapa
      //dentro de otro mapa.
      return {'ok': false, 'mensaje': decodedResp['error']['message']};
    }
  }

  Future<Map<String, dynamic>> nuevoUsuario( String email, String password) async {
    final authData = {
      'email': email,
      'password': password,
      'returnSecureToken': true
    };

    final resp = await http.post(
        'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=$_firebaseToken',
        body: json.encode(authData));
    Map<String, dynamic> decodedResp = json.decode(resp.body);
    print(decodedResp);

    if (decodedResp.containsKey('idToken')) {
    
      //salvar el token en el storage
      _prefs.token = decodedResp['idToken'];

      //si es verdadero, retorna un ok, y de paso muestra el idToken.
      return {'ok': true, 'token': decodedResp['idToken']};
    } else {
      //Caso contrario, muestra un false y en lugar del token muestra un
      //mensaje con el error, ya que dentro del objeto de la respuesta en su
      //propiedad error, buscara algo llamado message, ya que uno es un mapa
      //dentro de otro mapa.
      return {'ok': false, 'mensaje': decodedResp['error']['message']};
    }
  }
}
