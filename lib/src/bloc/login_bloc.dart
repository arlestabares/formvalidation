import 'dart:async';
// import 'package:flutter_observable_state/flutter_observable_state.dart';
import 'package:formvalidation/src/bloc/validator.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc with Validators {
  //NOTA: Los streamController <>(), no son conocidos en la libreria
  //import 'package:rxdart/rxdart.dart';..pues no existen esos objetos.
  //Pero lo que si existen son los pues no existen esos objetos.
  //Pero lo que si existen son lospues no existen esos objetos.
  //Pero lo que si existen son los  BehaviorSubject<String>();...estos
  //tambien tienen su .stream.transform();, su broadCast()

  // final _emailController    = StreamController<String>.broadcast();
  // final _passwordController = StreamController<String>.broadcast();

  final _emailController    = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();

//Recuperar los valores del Stream, ó escuchar los valores ingresados en
//la cajonera del email y el password.
//NOTA: Estos metodos son una conveniencia para no tener que apuntar siempre
// a metodos con instancias , en cambio solo se hara la llamada a cada uno de
//los metodos siguientes. que son:

  //Stream<String> get emailStream     => _emailController.stream;
  //Stream<String> get passwordStream => _passwordController.stream;

  //Nota: El _passwordController.stream.transform(validarPassword), lo acabo de transformar
  //agregandole el .transfor(validarPassword), cuyo transform viene de la clase Validator().
  Stream<String> get passwordStream =>
      _passwordController.stream.transform(validarPassword);
  Stream<String> get emailStream =>
      _emailController.stream.transform(validarEmail);

  // Stream<bool> get formValidStream =>
  //     Observable.combineLatest2(emailStream, passwordStream, (e, p) => true);

    Stream<bool> get formValidStream => 
    Observable.combineLatest2(emailStream, passwordStream, (a, b) => true);



//Insertar valores al Stream
  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;
  
  
  //Obtener ultimo valor ingresado a los Stringss
  String get email    => _emailController.value;
  String get password => _passwordController.value;
  

//La traduccion del metodo dispose() seria, disponer, colocar, decidir.
//Necesario para cerrar los stream y que no generen error
  dispose() {
    _emailController?.close();
    _passwordController?.close();
  }
}
