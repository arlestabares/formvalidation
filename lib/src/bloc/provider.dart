import 'package:flutter/material.dart';

import 'package:formvalidation/src/bloc/login_bloc.dart';
export 'package:formvalidation/src/bloc/login_bloc.dart';

import 'package:formvalidation/src/bloc/productos_bloc.dart';
export 'package:formvalidation/src/bloc/productos_bloc.dart';


class Provider extends InheritedWidget {

 final loginBloc = new LoginBloc();
 final _productosBloc = new ProductosBloc();
  static Provider _instancia;

  factory Provider({Key key, Widget child}) {
  
  
    if (_instancia == null) {
      _instancia = new Provider._internal(key: key, child: child);
    }
    return _instancia;
  }

  Provider._internal({Key key, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
  
  
//NOTA: Esta funcion va a buscar internamente en ese gran arbol de Widgets
//que viene en el context,y busca un Widget exactamente con el mismo nombre
//que Provider y va a retornarme la instancia del LoginBloc(),
//que defini como loginBloc = LoginBloc().basado en este contexto.
  static LoginBloc of(BuildContext context) {
    return (context.dependOnInheritedWidgetOfExactType<Provider>()).loginBloc;
    //return  (context.inheritFromWidgetOfExactType(Provider)as Provider).loginBloc;
  }
  
  //NOTA: Esta funcion va a buscar internamente en ese gran arbol de Widgets
//que viene en el context,y busca un Widget exactamente con el mismo nombre
//que Provider y lo trate como la clase Provider, que va a retornarme la instancia 
//de _productosBloc(),
//que defini como _productosBloc = new ProductosBloc().basado en este contexto.
  static ProductosBloc productosBloc(BuildContext context) {
    return (context.dependOnInheritedWidgetOfExactType<Provider>())._productosBloc;
    //return  (context.inheritFromWidgetOfExactType(Provider)as Provider).loginBloc;
  }
  
  
  
}
