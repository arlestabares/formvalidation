import 'dart:io';

import 'package:formvalidation/src/models/producto_model.dart';
import 'package:formvalidation/src/provider/productos_provider.dart';
import 'package:rxdart/rxdart.dart';

class ProductosBloc {
  final _productosController = new BehaviorSubject<List<ProductoModel>>();
  final _cargandoController = new BehaviorSubject<bool>();

//Creamos una referencia a los productosProvider, para poder hacer la peticion
//a cada uno de esos procesos.

  final _productosProvider = new ProductosProvider();

//Ahora necesitop escuchar los stream de los controller anteriores, el objetivo
// de la siguiente funcion es reducir el codigo, para que sea mas facil de implementar
//en donde sea que trabajemos el productoBloc.

  Stream<List<ProductoModel>> get productosStream =>
      _productosController.stream;
  Stream<bool> get cargando => _cargandoController.stream;

  void cargarProductos() async {
    final productos =
        await _productosProvider.cargarProductosDeFirebaseEnMovil();
    _productosController.sink.add(productos);
  }
  
  
    void agregarProducto(ProductoModel producto) async {
      //El _cargandoController  lo utilizo para saber cuándo estoy cargando
      //un producto. .esto nos sirve por si hay que bloquear botones o algo
      //parecido
      _cargandoController.sink.add(true);
      await _productosProvider.agregarProducto(producto);
      _cargandoController.sink.add(false);
    }
    
    
    Future <String>  subirFoto(File foto) async {
      //El _cargandoController  lo utilizo para saber cuándo estoy cargando
      //un producto. .esto nos sirve por si hay que bloquear botones o algo
      //parecido
      _cargandoController.sink.add(true);
     final fotoUrl = await  _productosProvider.subirImagen(foto);
      _cargandoController.sink.add(false);
      
      return fotoUrl;
    }
    
    
    void editarProducto(ProductoModel producto) async {
      //El _cargandoController  lo utilizo para saber cuándo estoy cargando
      //un producto. .esto nos sirve por si hay que bloquear botones o algo
      //parecido
      _cargandoController.sink.add(true);
      await _productosProvider.editarProductoEnFirebase(producto);
      _cargandoController.sink.add(false);
    }
    
    
    
    void borrarProducto(String id ) async {
      //El _cargandoController  lo utilizo para saber cuándo estoy cargando
      //un producto. .esto nos sirve por si hay que bloquear botones o algo
      //parecido
    
      await _productosProvider.borrarProductosDeFireBase(id);
   
    }
  

  dispose() {
//Si _productosController esta cargado, entonces cierrelo
//Si _cargandoController esta cargado, entonces cierrelo
    _productosController?.close();
    _cargandoController?.close();
  }
}
