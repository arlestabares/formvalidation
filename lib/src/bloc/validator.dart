import 'dart:async';

class Validators {



//Al StreanTransformer le llega un String de entrada y bota de salida
//otro String en este caso. aunque no es obligatorio especificarlo,
//lo mejor es dejarlo definido.
//NOTA: Esta funcion la puedo utilizar en cualquier clase que requiera 
//de ella, En estecaso sera la clase LoginBloc(), que tiene los stream
//para la verificacion de, si es correcta o no la entrada de datos
//por parte del usuario.
  final validarPassword = StreamTransformer<String, String>.fromHandlers(
      handleData: (password, sink) {
      
      if (password.length >= 6) {
        sink.add(password);
      } else {
      sink.addError('Mas de 6 caracteres por favor');
      }
      
      }
   );
   
   
   
   
    final validarEmail = StreamTransformer<String, String>.fromHandlers(
      handleData: (email, sink) {
      
     Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'; 
      RegExp regExp  = new RegExp(pattern);
      
      
      if (regExp.hasMatch(email)) {
        sink.add(email);
      } else {
      sink.addError('Email Incorrecto');
      }
      
      }
   );
}
