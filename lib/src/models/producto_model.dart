// To parse this JSON data, do
//
//     final productoModel = productoModelFromJson(jsonString);

import 'dart:convert';

ProductoModel productoModelFromJson(String str) => ProductoModel.fromJson(json.decode(str));

//Funcion que regresa el modelo como un string y ademas esta Global , asi que 
//se puede llamar en cualqier clase que lo requiera.
String productoModelToJson(ProductoModel data) => json.encode(data.toJson());

class ProductoModel {

    //Definicion del modelo con todas sus propiedades
    String id;
    String titulo;
    double valor;
    bool disponible;
    String fotoUrl;

    //Constructor  que inicializa los propiedades 
    ProductoModel({
        this.id,
        this.titulo = '',
        this.valor = 0.0,
        this.disponible = true,
        this.fotoUrl ,
    });
    
    //Aqui el fromJson recibe un Mapa  llamado json, el cual asigna los 
    //valores que vienen en ese mapa a cada una de las propiedades
    //del modelo definido .
    //Este factory constructor retorna una nueva instancia 
    //del ProductoModel cuando se invoque. 
    factory ProductoModel.fromJson(Map<String, dynamic> json) => ProductoModel(
        id         : json["id"],
        titulo     : json["titulo"],
        valor      : json["valor"],
        disponible : json["disponible"],
        fotoUrl    : json["fotoUrl"],
    );

    //Aqui tenemos el proceso inverso, en el Mapa  tomamos el 
    //modelo y lo transforma a un Json
    Map<String, dynamic> toJson() => {
        // "id"         : id,
        "titulo"     : titulo,
        "valor"      : valor,
        "disponible" : disponible,
        "fotoUrl"    : fotoUrl,
    };
}
